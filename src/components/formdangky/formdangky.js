	// Start: formdangky
	$("#form-registration").validate({
	onfocusout: false,
	onkeyup: false,
	onclick: false,
	rules: {
		"user": {
				required: true,
		},
		"email":{
				required: true,
				email: true,
		},
		"matkhau":{
			require: true,
		},
		"re-matkhau":{
			require: true,
			equalTo: "#password-input",
		},
		"tendaydu":{
			require: true,
		},
		"ngaysinh":{
			require: true,
		},
		"gioitinh":{
			require: true,
		},
		"donvi":{
			require: true,
		},
	},
	messages: {
		"user": {
			required: "* Bắt buộc nhập tên đăng nhập.",
			maxlength: jQuery.validator.format("* Lớn nhất có 64 kí tự"),
			minlength: jQuery.validator.format("* Ít nhất phải có 6 kí tự"),
		},
		"email": {
			required: "* Bắt buộc nhập email.",
			email: "* Email của bạn sai định dạng.",
		},
		"matkhau": {
			require: "* Nhập mật khẩu đăng nhập.",
			maxlength: jQuery.validator.format("* Lớn nhất có 64 kí tự"),
			minlength: jQuery.validator.format("* Ít nhất phải có 6 kí tự"),
		},
		"re-matkhau":{
			require: "* Nhập lại mật khẩu đăng nhập.",
			equalTo: "* Phải giống mật khẩu."
		},
		"tendaydu": {
			require: "* Nhập tên của bạn.",
		},
		"ngaysinh": {
			require: "* Nhập ngày tháng năm sinh.",
		},
		"gioitinh": {
			require: "* Chọn giới tính.",
		},
		"donvi": {
			require: "* Chọn đơn vị.",
		},
		"code-comfirm": {
			require: "* Nhập mã xác nhận.",
		}
	}
	});
	// End: formdangky
