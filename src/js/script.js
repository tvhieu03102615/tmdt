"use strict";
jQuery(document).ready(function($) {
    // Window on load
    $(window).on('load', function() {
        $("#select").chosen();
        $(".select__top .chosen-container-single").css("width", "100%");
        // Khanh js
        // validate form dang ky
        $("#form-registration").validate({
            onfocusout: false,
            onkeyup: false,
            onclick: false,
            rules: {
                "user": {
                    required: true,
                },
                "email": {
                    required: true,
                    email: true,
                },
                "matkhau": {
                    require: true,
                },
                "re-matkhau": {
                    require: true,
                    equalTo: "#password-input",
                },
                "tendaydu": {
                    require: true,
                },
                "ngaysinh": {
                    require: true,
                },
                "gioitinh": {
                    require: true,
                },
                "donvi": {
                    require: true,
                },
            },
            messages: {
                "user": {
                    required: "* Bắt buộc nhập tên đăng nhập.",
                    maxlength: jQuery.validator.format("* Lớn nhất có 64 kí tự"),
                    minlength: jQuery.validator.format("* Ít nhất phải có 6 kí tự"),
                },
                "email": {
                    required: "* Bắt buộc nhập email.",
                    email: "* Email của bạn sai định dạng.",
                },
                "matkhau": {
                    require: "* Nhập mật khẩu đăng nhập.",
                    maxlength: jQuery.validator.format("* Lớn nhất có 64 kí tự"),
                    minlength: jQuery.validator.format("* Ít nhất phải có 6 kí tự"),
                },
                "re-matkhau": {
                    require: "* Nhập lại mật khẩu đăng nhập.",
                    equalTo: "* Phải giống mật khẩu."
                },
                "tendaydu": {
                    require: "* Nhập tên của bạn.",
                },
                "ngaysinh": {
                    require: "* Nhập ngày tháng năm sinh.",
                },
                "gioitinh": {
                    require: "* Chọn giới tính.",
                },
                "donvi": {
                    require: "* Chọn đơn vị.",
                },
                "code-comfirm": {
                    require: "* Nhập mã xác nhận.",
                }
            }
        });
        // $("#form-login").validate({
        // onfocusout: false,
        // onkeyup: false,
        // onclick: false,
        // rules: {
        //     "taikhoan": {
        //         required: true,
        //     },
        //     "matkhau":{
        //         required: true,
        //     },
        // },
        // messages: {

        // }
        // });

        // --------> form dang nhap - remember - click
        $(".remember").click(function(event) {
            $(".choose").toggleClass('active');
        });
         

        // ------------ MENU ------------------ //
        var scrollTop;
        $(window).scroll(function(event) {
            $("#menu__header").removeClass('active-menu');
            $(".menu-level2").removeClass('active');
            scrollTop = $(window).scrollTop();
        });
        // --------> click menu block
        $(".menu-res-bars").click(function(event) {
            $("#menu__header").toggleClass('active-menu');
            $(".menu-level2").removeClass('active');
            $(".menu-level2").slideUp();
            $(".icon-dropdown, .icon-dropdown2").removeClass('active-click');
        });
        // ---------> scroll + resize
        // --------> menu
        $(".icon-dropdown").click(function(event) {
            // $(".icon-dropdown").removeClass('active-click');
            if ($(window).width() < 981 && $(window).width() > 568) {
                event.stopPropagation();
                // icon rotate
                $(this).siblings().removeClass('active-click');
                $(this).toggleClass('active-click');
                // menu
                if ($(this).children(".menu-level2").hasClass('active')) {
                    $(this).siblings().children(".menu-level2").removeClass('active');
                    $(this).children(".menu-level2").removeClass('active');
                } else {
                    $(this).siblings().children(".menu-level2").removeClass('active');
                    $(this).children(".menu-level2").addClass('active');
                }
                $(this).siblings().children(".menu-level2 .menu-level2").slideUp();
                $(this).children(".menu-level2 .menu-level2").slideToggle();
            }
            if ($(window).width() < 569) {
                event.stopPropagation();
                // icon rotate
                $(this).siblings().removeClass('active-click');
                $(this).toggleClass('active-click');
                // menu
                if ($(this).children(".menu-level2").hasClass('active')) {
                    $(this).siblings().children(".menu-level2").removeClass('active');
                    $(this).children(".menu-level2").removeClass('active');
                } else {
                    $(this).siblings().children(".menu-level2").removeClass('active');
                    $(this).children(".menu-level2").addClass('active');
                }
                $(this).siblings().children(".menu-level2").slideUp();
                $(this).children(".menu-level2").slideToggle();
            }
        });
        $(window).resize(function(event) {
            if ($(window).width() > 980) {
                $("#menu__header").css("display", "block");
            }
        }); 
            //start js by Tuan
            //js for advs-company
            console.log("JS cua Tuan");
            $(".advs-company__content,.featured-news__list").mCustomScrollbar({
                theme: "customByTuan"
            });
            //js for feature-news__desc
            var countLi = $(".featured-news__list li").length;
            console.log(countLi);
            var i = 2;
            setInterval(function() {
                console.log("HIHI");
                $(".featured-news__list li").removeClass('general-item--active');
                var selectorLi = ".featured-news__list li:nth-child(" + i + ")";
                console.log("Chuoi: " + selectorLi);
                $(selectorLi).addClass('general-item--active');
                i++;
                if (i == countLi + 1) {
                    i = 1;
                }
            }, 2000);
            // js for classified-ad
            $('.classified-ad__caregory a').click(function(e) {
                e.preventDefault();
                $('.classified-ad__caregory a').removeClass('classified-ad__caregory--active');
                $(this).addClass('classified-ad__caregory--active');
                $(this).addClass('xxxxxxxxx');
                $('.classified-ad__list').removeClass('classified-ad__list--active');
                var currentID = "#" + $(this).attr('id');
                console.log("In ra nè: " + currentID);
                if (currentID == "#btn-advs-space") {
                    $('#advs-space.classified-ad__list').addClass('classified-ad__list--active');
                } else if (currentID == "#btn-advs-internet") {
                    $('#advs-internet.classified-ad__list').addClass('classified-ad__list--active');
                }
                //js for home_l__box4
                $('.home_l__box4__nav a').click(function(e) {
                    e.preventDefault();
                    $('.home_l__box4__nav a').removeClass('active');
                    $(this).addClass('active');
                    var indexTag = $(this).index() + 1;
                    $('.home_l__box4__content').removeClass('home_l__box4__content--active');
                    if (indexTag == 1) {
                        $('#highlights-post.home_l__box4__content').addClass('home_l__box4__content--active');
                    } else if (indexTag == 2) {
                        $('#news.home_l__box4__content').addClass('home_l__box4__content--active');
                    } else if (indexTag == 3) {
                        $('#market.home_l__box4__content').addClass('home_l__box4__content--active');
                    } else if (indexTag == 4) {
                        $('#tendency.home_l__box4__content').addClass('home_l__box4__content--active');
                    } else if (indexTag == 5) {
                        $('#knowlegde.home_l__box4__content').addClass('home_l__box4__content--active');
                    }
                });
                // end js by Tuan
            });
    });
});
//js nha moi gioi ____chuc
function openPage(pageName) {
    var i, pagenone;
    pagenone = document.getElementsByClassName("pagenone");
    for (i = 0; i < pagenone.length; i++) {
        pagenone[i].style.display = "none";
    }
    document.getElementById(pageName).style.display = "block";
    // pageName.currentTarget.className += " bg-yellow";
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("Open").click();